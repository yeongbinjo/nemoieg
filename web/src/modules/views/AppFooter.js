import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Typography } from "@material-ui/core";
import FacebookLogo from "../../images/facebook.webp";
import LinkedInLogo from "../../images/linkedin.webp";
import SocialLogo from "../../images/generic-social-link.webp";
import Link from "@material-ui/core/Link";

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: 200,
    color: 'rgba(255,255,255,0.7)',
    backgroundColor: '#808080',
    padding: `${theme.spacing(4)}px ${theme.spacing(3)}px`,
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
      padding: `${theme.spacing(3)}px ${theme.spacing(8)}px`,
    },
  },
  left: {
    flex: '1 1 auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    [theme.breakpoints.up('sm')]: {
      flex: '1 1 70%',
    }
  },
  right: {
    flex: '1 1 30%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    [theme.breakpoints.down('sm')]: {
      flex: '1 1 auto',
      marginTop: theme.spacing(2),
    },
    '& a': {
      margin: theme.spacing(1),
    },
    [theme.breakpoints.down('xs')]: {
      '& img': {
        width: 24,
        height: 24,
      }
    }
  },
  info: {
    '& dl': {
      display: 'inline-block',
      paddingRight: theme.spacing(2),
      margin: 0,
    },
    '& dl > dt': {
      display: 'inline-block',
      color: 'rgba(255,255,255,0.88)',
      fontWeight: 700,
      paddingRight: 4,
    },
    '& dl > dd': {
      margin: 0,
      display: 'inline-block',
    },
  },
  address: {
    [theme.breakpoints.down('sm')]: {
      marginTop: theme.spacing(2),
    },
    '& dl': {
      display: 'inline-block',
      paddingRight: theme.spacing(2),
      margin: 0,
    },
    '& dl > dt': {
      display: 'inline-block',
      color: 'rgba(255,255,255,0.88)',
      fontWeight: 700,
      paddingRight: 4,
    },
    '& dl > dd': {
      margin: 0,
      display: 'inline-block',
    },
    '& address': {
      fontStyle: 'normal',
    }
  }
}));

export default function AppFooter() {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <div className={classes.root}>
      <div className={classes.left}>
        <div className={classes.info}>
          <dl>
            <dt>사업자등록번호</dt>
            <dd>230-81-08091</dd>
          </dl>
          <dl>
            <dt>상호</dt>
            <dd>주식회사 네모아이이지</dd>
          </dl>
          <dl>
            <dt>전화번호</dt>
            <dd>02-423-0905 | 02-3401-7001</dd>
          </dl>
        </div>

        <div className={classes.address}>
          <dl>
            <dt>주소</dt>
            <dd>
              <address>서울특별시 강남구 테헤란로 521 30층 (삼성동, 파르나스타워) 우편번호 06164</address>
            </dd>
          </dl>
        </div>

        <Typography variant="body1" style={{ marginTop: theme.spacing(2), color: 'rgba(255,255,255,0.88)' }}>
          &copy; 2019 Nemo IEG
        </Typography>
      </div>
      <div className={classes.right}>
        <Link target="_blank" href="https://www.facebook.com/OperationConsulting/" rel="noopener noreferrer">
          <img src={FacebookLogo} alt="facebook" />
        </Link>
        <Link target="_blank" href="https://www.linkedin.com/company/nemo-ieg/" rel="noopener noreferrer">
          <img src={LinkedInLogo} alt="linkedin" />
        </Link>
        <Link target="_blank" href="https://m.post.naver.com/my.nhn?memberNo=44008149" rel="noopener noreferrer">
          <img src={SocialLogo} alt="naver" />
        </Link>
      </div>
    </div>
  );
}
