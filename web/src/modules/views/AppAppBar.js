import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '../components/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
import Logo from '../../images/ieg_logo.png';
import { Link } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  logo: {
    display: 'block',
    height: 32,
    marginLeft: theme.spacing(1),
    [theme.breakpoints.up('sm')]: {
      height: 40,
      marginLeft: theme.spacing(4),
    },
  },
  rightLink: {
    fontSize: 15,
    color: theme.palette.common.black,
    margin: theme.spacing(2),
    textDecoration: 'none',
    fontWeight: 800,
    textTransform: 'uppercase',
  },
  toolbar: {
    minHeight: 56,
    [theme.breakpoints.up('sm')]: {
      minHeight: 70
    },
  }
}));

export default function AppAppBar() {
  const classes = useStyles();
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  function handleMobileMenuClose() {
    setMobileMoreAnchorEl(null);
  }

  function handleMobileMenuOpen(event) {
    setMobileMoreAnchorEl(event.currentTarget);
  }

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <Link
          className={classes.rightLink}
          to="/about/"
        >
          {'About Nemo IEG'}
        </Link>
      </MenuItem>
      <MenuItem>
        <Link
          className={classes.rightLink}
          to="/what-we-do/"
        >
          {'What We Do'}
        </Link>
      </MenuItem>
      <MenuItem>
        <Link
          className={classes.rightLink}
          to="/cases/"
        >
          {'Case & Insights'}
        </Link>
      </MenuItem>
      <MenuItem>
        <Link
          className={classes.rightLink}
          to="/career/"
        >
          {'Career'}
        </Link>
      </MenuItem>
      <MenuItem>
        <Link
          className={classes.rightLink}
          to="/contact/"
        >
          {'Contact Us'}
        </Link>
      </MenuItem>
    </Menu>
  );

  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar className={classes.toolbar}>
          <Link
            variant="h6"
            underline="none"
            color="inherit"
            to="/"
          >
            <img src={Logo} alt="logo" className={classes.logo} />
          </Link>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <Link
              className={classes.rightLink}
              to="/about/"
            >
              {'About Nemo IEG'}
            </Link>
            <Link
              className={classes.rightLink}
              to="/what-we-do/"
            >
              {'What We Do'}
            </Link>
            <Link
              className={classes.rightLink}
              to="/cases/"
            >
              {'Case & Insights'}
            </Link>
            <Link
              className={classes.rightLink}
              to="/career/"
            >
              {'Career'}
            </Link>
            <Link
              className={classes.rightLink}
              to="/contact/"
            >
              {'Contact Us'}
            </Link>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
    </div>
  );
}