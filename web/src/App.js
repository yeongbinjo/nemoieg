import withRoot from './modules/withRoot';
import React from 'react';
import AppAppBar from "./modules/views/AppAppBar";
import NotFound from "./NotFound";
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import Main from "./Main";
import AppFooter from "./modules/views/AppFooter";
import Cases from "./Cases";
import About from "./About";
import What from "./What";
import Career from "./Career";
import Contact from "./Contact";

function Index() {
  return (
    <React.Fragment>
      <BrowserRouter>
        <AppAppBar />
        <Switch>
          <Route exact path="/" component={Main} />
          <Route exact path="/about/" component={About} />
          <Route exact path="/what-we-do/" component={What} />
          <Route exact path="/cases/" component={Cases} />
          <Route exact path="/career/" component={Career} />
          <Route exact path="/contact/" component={Contact} />
          <Route component={NotFound} />
        </Switch>
      </BrowserRouter>
      <AppFooter />
    </React.Fragment>
  );
}

export default withRoot(Index);
