import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faChevronLeft, faSignOutAlt, faSync, faUserPlus,
} from '@fortawesome/free-solid-svg-icons'

const loadFontAwesome = () => {
  library.add(faSync);
  library.add(faChevronLeft);
  library.add(faSignOutAlt);
  library.add(faUserPlus);
};

export default loadFontAwesome;
