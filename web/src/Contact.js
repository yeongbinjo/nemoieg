import React from 'react';
import { makeStyles, useTheme, withStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import FacebookLogo from "./images/facebook.webp";
import LinkedInLogo from "./images/linkedin.webp";
import SocialLogo from "./images/generic-social-link.webp";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { grey } from "@material-ui/core/colors";
import { GoogleMap, LoadScript, Marker, InfoWindow } from '@react-google-maps/api'
import useMediaQuery from "@material-ui/core/useMediaQuery/useMediaQuery";

const useStyles = makeStyles(theme => ({
  section: {
    backgroundColor: '#03013A',
    position: 'relative',
    backgroundSize: 'cover',
    minHeight: 'calc(100vh - 256px)',
    [theme.breakpoints.up('sm')]: {
      minHeight: 'calc(100vh - 270px)'
    }
  },
  social: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    '& img': {
      width: 28,
      height: 28,
    },
    [theme.breakpoints.down('sm')]: {
      flex: '1 1 auto',
      marginTop: theme.spacing(2),
    },
    '& a': {
      margin: theme.spacing(1),
    },
    [theme.breakpoints.down('xs')]: {
      '& img': {
        width: 20,
        height: 20,
      }
    }
  },
  contact: {
    paddingTop: theme.spacing(14),
    paddingBottom: theme.spacing(14),
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    textAlign: 'center',
    color: 'white',
    '& p': {
      fontSize: 18,
      fontWeight: 800,
      paddingTop: 4,
      paddingBottom: 4,
    },
    '& h1': {
      color: 'white',
      fontSize: 52,
      fontWeight: 800,
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(3),
    },
    [theme.breakpoints.down('sm')]: {
      paddingTop: theme.spacing(2),
      paddingBottom: theme.spacing(12),
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
    }
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  input: {
    borderRadius: 0,
    position: 'relative',
    color: 'white',
    fontSize: 16,
    width: 'auto',
    '& input, & textarea': {
      color: 'white'
    },
    '& ::placeholder': {
      opacity: 1,
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderRadius: 0,
        borderColor: 'white',
      },
      '&:hover fieldset': {
        borderColor: grey[400],
      },
      '&.Mui-focused fieldset': {
        borderColor: 'white',
      },
    },
  },
  button: {
    height: 56,
    width: 150,
    margin: '-26px auto',
  },
  map: {
    paddingTop: theme.spacing(14),
    paddingBottom: theme.spacing(14),
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    [theme.breakpoints.down('sm')]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
    }
  }
}));

const WhiteButton = withStyles(theme => ({
  root: {
    borderRadius: 0,
    color: theme.palette.getContrastText(theme.palette.common.white),
    backgroundColor: theme.palette.common.white,
    '&:hover': {
      backgroundColor: grey[200]
    },
  },
}))(Button);

export default function Contact() {
  const classes = useStyles();
  const theme = useTheme();
  const sm = useMediaQuery(theme.breakpoints.down('sm'));
  const height = sm ? 400 : 700;

  return (
    <React.Fragment>
      <div className={classes.section}>
        <Container maxWidth="lg">
          <Grid container>
            <Grid item xs={12} md={6} className={classes.map}>
              <LoadScript
                id="script-loader"
                googleMapsApiKey="AIzaSyDDyfUAfLsxealF_bvUQDDV6BdKGAtQTIc"
              >
                <GoogleMap
                  id='nemo-ieg-map'
                  mapContainerStyle={{
                    height: `${height}px`,
                    width: "100%"
                  }}
                  zoom={14}
                  center={{
                    lat: 37.5092733,
                    lng: 127.0606442,
                  }}
                >
                  <div>
                    <Marker
                      position={{
                        lat: 37.5092733,
                        lng: 127.0606442,
                      }}
                    />
                    <InfoWindow
                      position={{lat: 37.5122071, lng: 127.0606442}}
                    >
                      <div style={{
                        background: `white`,
                        fontFamily: '"NanumSquare"',
                        fontWeight: 700,
                      }}>
                        {'서울시 강남구 테헤란로 521 파르나스타워 30층'}
                      </div>
                    </InfoWindow>
                  </div>
                </GoogleMap>
              </LoadScript>
            </Grid>
            <Grid item xs={12} md={6} className={classes.contact}>
              <Typography variant={'h1'}>CONTACT US</Typography>
              <Typography variant={'body1'}>서울시 강남구 테헤란로 521</Typography>
              <Typography variant={'body1'}>파르나스 타워 30층</Typography>
              <Typography variant={'body1'} style={{marginTop: 12, marginBottom: 12}}>hwlee@nemopartners.com</Typography>
              <Typography variant={'body1'}>02-423-0905</Typography>
              <Typography variant={'body1'}>02-3404-7001</Typography>

              <div className={classes.social}>
                <Link target="_blank" href="https://www.facebook.com/OperationConsulting/" rel="noopener noreferrer">
                  <img src={FacebookLogo} alt="facebook" />
                </Link>
                <Link target="_blank" href="https://www.linkedin.com/company/nemo-ieg/" rel="noopener noreferrer">
                  <img src={LinkedInLogo} alt="linkedin" />
                </Link>
                <Link target="_blank" href="https://m.post.naver.com/my.nhn?memberNo=44008149" rel="noopener noreferrer">
                  <img src={SocialLogo} alt="naver" />
                </Link>
              </div>

              <div className={classes.form}>
                <TextField
                  id="name"
                  required
                  placeholder="이름 *"
                  className={classes.input}
                  fullWidth
                  margin="dense"
                  variant="outlined"
                />
                <TextField
                  id="email"
                  required
                  placeholder="이메일 *"
                  className={classes.input}
                  fullWidth
                  margin="dense"
                  variant="outlined"
                />
                <TextField
                  id="title"
                  placeholder="제목"
                  className={classes.input}
                  fullWidth
                  margin="dense"
                  variant="outlined"
                />
                <TextField
                  id="message"
                  placeholder="메세지"
                  className={classes.input}
                  margin="dense"
                  variant="outlined"
                  multiline
                  fullWidth
                  rows="5"
                  rowsMax="5"
                />
                <WhiteButton className={classes.button}>보내기</WhiteButton>
              </div>
            </Grid>
          </Grid>
        </Container>
      </div>
    </React.Fragment>
  )
}
