import React from 'react';
import { Parallax } from "react-parallax";
import { makeStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import cls from 'classnames';
import firstBg from './images/1.webp';
import secondBg from './images/2.webp';
import thirdBg from './images/3.webp';

const useStyles = makeStyles(theme => ({
  white: {
    color: theme.palette.common.white,
  },
  content: {
    backgroundColor: 'rgba(0,0,0,0.38)',
    position: 'absolute',
    width: '90%',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)',
    padding: theme.spacing(4),
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(8),
    },
  },
  firstH1: {
    [theme.breakpoints.up('sm')]: {
      marginTop: theme.spacing(2),
      fontSize: 88,
      fontWeight: 800,
      textTransform: 'none',
    }
  },
  firstH4: {
    fontSize: 24,
    fontWeight: 800,
    textTransform: 'none',
  },
  firstBody: {
    marginTop: theme.spacing(3),
    lineHeight: '1.75em',
    [theme.breakpoints.up('sm')]: {
      fontWeight: 800,
      marginTop: theme.spacing(6),
      fontSize: 22,
    },
  },
  firstBody2: {
    lineHeight: '1.75em',
    [theme.breakpoints.up('sm')]: {
      fontWeight: 800,
      fontSize: 22,
    },
  },
  second: {
    position: 'relative',
    backgroundImage: 'url("' + secondBg + '")',
    backgroundSize: 'cover',
    height: 500,
    [theme.breakpoints.up('sm')]: {
      height: 700,
    }
  },
  secondContent: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    display: 'flex',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    transform: 'none',
    flexDirection: 'column',
    [theme.breakpoints.up('md')]: {
      flexDirection: 'row',
    },
  },
  secondLeft: {
    flex: '1 1 50%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    [theme.breakpoints.up('md')]: {
      flex: '0 0 400px',
    }
  },
  secondRight: {
    flex: '1 1 50%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    [theme.breakpoints.up('md')]: {
      flex: '1 1 auto',
    }
  },
  secondH1: {
    [theme.breakpoints.up('sm')]: {
      fontSize: 70,
      fontWeight: 800,
    }
  },
  secondBody1: {
    fontWeight: 400,
    [theme.breakpoints.up('sm')]: {
      fontSize: 32,
      lineHeight: 'normal',
    },
  },
  secondBody2: {
    marginTop: theme.spacing(2),
    color: '#bbb',
    fontWeight: 400,
    [theme.breakpoints.up('sm')]: {
      marginTop: theme.spacing(4),
      fontSize: 32,
      lineHeight: 'normal',
    },
  },
  secondBody3: {
    color: '#bbb',
    fontWeight: 400,
    [theme.breakpoints.up('sm')]: {
      fontSize: 30,
      lineHeight: 'normal',
    },
  },

  third: {
    position: 'relative',
    backgroundImage: 'url("' + thirdBg + '")',
    backgroundSize: 'cover',
    height: 350,
    [theme.breakpoints.up('sm')]: {
      height: 700,
    }
  },
  thirdContent: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    display: 'flex',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    transform: 'none',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(3),
  },
  thirdH1: {
    textAlign: 'center',
    fontWeight: 'normal',
    fontSize: 24,
    lineHeight: 'normal',
    color: '#bbb',
    [theme.breakpoints.up('sm')]: {
      fontSize: 50,
    }
  }
}));

export default function Main() {
  const classes = useStyles();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('sm'));
  const height = matches ? 1100 : 700;

  return (
    <React.Fragment>
      <Parallax
        bgImage={firstBg}
        bgImageAlt="first Image"
        strength={height}
      >
        <div style={{ height: `${height}px` }}>
          <div className={classes.content}>
            <Typography variant="h1" className={cls(classes.white, classes.firstH1)}>
              {'IEG'}
            </Typography>
            <Typography variant="h4" className={cls(classes.white, classes.firstH4)}>
              {'Industry Experts Group'}
            </Typography>
            <Typography variant="body1" className={cls(classes.white, classes.firstBody)}>
              {'"실제적인 고객가치 증대를 위한 Operation 컨설팅 전문기업입니다.'}
            </Typography>
            <Typography variant="body1" className={cls(classes.white, classes.firstBody2)}>
              {'산업전문가(Industry Expert)로서 재무적 효과로 연결되는 현장 중심의 기업가치 제고와 역량 강화 서비스를 제공하고 있습니다."'}
            </Typography>
          </div>
        </div>
      </Parallax>
      
      <div className={classes.second}>
        <div className={cls(classes.content, classes.secondContent)}>
          <div className={classes.secondLeft}>
            <Typography variant="h1" className={cls(classes.white, classes.secondH1)}>
              {'IEG'}
            </Typography>
            <Typography variant="h1" className={cls(classes.white, classes.secondH1)}>
              {'Vision'}
            </Typography>
          </div>
          <div className={classes.secondRight}>
            <Typography variant="body1" className={cls(classes.white, classes.secondBody1)}>
              {'"We are the partners providing Tangible Value to client."'}
            </Typography>
            <Typography variant="body1" className={cls(classes.white, classes.secondBody2)}>
              {'고객이 직면한 어려운 문제를 함께 풀어가는 파트너로서,'}
            </Typography>
            <Typography variant="body1" className={cls(classes.white, classes.secondBody3)}>
              {'​고객 관점에서 정량적으로 증명되는 가치개선을 추구합니다.'}
            </Typography>
          </div>
        </div>
      </div>

      <div className={classes.third}>
        <div className={cls(classes.content, classes.thirdContent)}>
          <Typography variant="h1" className={cls(classes.white, classes.thirdH1)}>
            {'IEG의 CONSULTING은 오로지 '}
            <span style={{ color: '#fff'}}>{'"결과"'}</span>
            {' 로 말합니다.'}
          </Typography>
        </div>
      </div>

    </React.Fragment>
  )
}
