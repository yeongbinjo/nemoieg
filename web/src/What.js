import React, { useEffect, useState } from 'react';
import { makeStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { useTheme, withStyles } from '@material-ui/core/styles';
import cls from 'classnames';
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import what0 from "./images/what0.webp";
import what1 from "./images/what1.webp";
import what2 from "./images/what2.webp";
import what3 from "./images/what3.webp";
import what4 from "./images/what4.webp";
import what5 from "./images/what5.webp";
import what6 from "./images/what6.webp";
import what7 from "./images/what7.webp";
import Button from "@material-ui/core/Button";
import thirdBg from "./images/3.webp";

const useStyles = makeStyles(theme => ({
  white: {
    color: theme.palette.common.white,
  },
  content: {
    backgroundColor: 'rgba(0,0,0,0.38)',
    position: 'absolute',
    width: '90%',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)',
    padding: theme.spacing(4),
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(8),
    },
  },
  header: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(6),
    '& h1': {
      fontSize: 24,
    },
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(8),
      '& h1': {
        fontSize: 30,
      },
    },
  },
  upperLine: {
    flex: '0 1 auto',
    fontSize: 50,
    color: '#03013A',
    textAlign: 'center',
    fontWeight: 800,
    lineHeight: '1.75em',
    '&:before': {
      content: '""',
      display: 'block',
      width: '70px',
      margin: 'auto',
      borderTop: '3px solid #03013A',
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: 32,
      '&:before': {
        width: '42px',
      }
    },
  },
  descriptionBox: {
    position: 'absolute',
    backgroundColor: 'rgba(0,0,0,0.55)',
    display: 'flex',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    color: 'white',
    '& h4': {
      color: 'white',
      fontSize: 28,
      fontWeight: 800,
    },
    '& ul': {
      paddingLeft: theme.spacing(3),
      marginTop: theme.spacing(8),
    },
    '& li': {
      marginTop: theme.spacing(8),
      marginBottom: theme.spacing(8),
      lineHeight: '2.3rem',
      fontSize: 20,
      fontWeight: 800,
    },
    minHeight: 400,
    [theme.breakpoints.down('xs')]: {
      '& h4': {
        fontSize: 24,
      },
      '& ul': {
        marginTop: theme.spacing(3),
      },
      '& li': {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        fontSize: 18,
      },
    },
  },
  description1: {
    minHeight: 650,
    position: 'relative',
    backgroundImage: 'url("' + what1 + '")',
    backgroundSize: 'cover',
    [theme.breakpoints.down('xs')]: {
      minHeight: 550,
    },
  },
  description2: {
    minHeight: 650,
    position: 'relative',
    backgroundImage: 'url("' + what2 + '")',
    backgroundSize: 'cover',
    [theme.breakpoints.down('xs')]: {
      minHeight: 550,
    },
  },
  description3: {
    minHeight: 650,
    position: 'relative',
    backgroundImage: 'url("' + what3 + '")',
    backgroundSize: 'cover',
    [theme.breakpoints.down('xs')]: {
      minHeight: 550,
    },
  },
  serviceBox: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
    padding: theme.spacing(3),
    '& img': {
      width: 220,
      height: 220,
      margin: theme.spacing(2),
      borderRadius: '50%',
    },
    '& h4': {
      fontSize: 28,
      color: '#03013A',
      fontWeight: 800,
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(3),
    },
    '& p': {
      color: '#03013A',
      fontSize: 20,
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      fontWeight: 400,
      maxWidth: '100%',
    },
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(1),
      '& p': {
        fontSize: 16,
      },
      '& h4': {
        fontSize: 20,
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
      },
    }
  },

  second: {
    position: 'relative',
    backgroundImage: 'url("' + what0 + '")',
    backgroundSize: 'cover',
    height: 200,
    [theme.breakpoints.up('sm')]: {
      height: 240,
    }
  },
  secondContent: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    display: 'flex',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    transform: 'none',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    '& h1': {
      fontSize: 50,
      fontWeight: 800,
    },
    [theme.breakpoints.down('xs')]: {
      '& h1': {
        fontSize: 32,
      },
    },
    [theme.breakpoints.up('md')]: {
      flexDirection: 'row',
    },
  },
  client: {
    textAlign: 'center',
    '& img': {
      width: '100%',
      maxWidth: 240,
      padding: theme.spacing(2),
    }
  },
  third: {
    position: 'relative',
    backgroundImage: 'url("' + thirdBg + '")',
    backgroundSize: 'cover',
    height: 350,
    [theme.breakpoints.up('sm')]: {
      height: 600,
    }
  },
  thirdContent: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    display: 'flex',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    transform: 'none',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(3),
  },
  thirdH1: {
    textAlign: 'center',
    fontWeight: 'normal',
    fontSize: 24,
    lineHeight: 'normal',
    color: '#bbb',
    [theme.breakpoints.up('sm')]: {
      fontSize: 50,
    }
  }
}));

const GhostButton = withStyles(theme => ({
  root: {
    borderRadius: 0,
    border: '1px solid',
    borderColor: '#000',
    fontWeight: 800,
    fontSize: 18,
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    transition: 'border-color 0.3s ease-in',
    '&:hover': {
      borderColor: 'rgba(0,0,0,0)',
      backgroundColor: '#fff',
    }
  },
}))(Button);

export default function What() {
  const classes = useStyles();
  const theme = useTheme();
  const [clients, updateClients] = useState([]);
  const url = '/api/client/';

  useEffect(() => {
    (async function () {
      const resp = await fetch(url);
      const json = await resp.json();
      updateClients(json);
    })();
  }, [url]);

  return (
    <React.Fragment>
      <div className={classes.second}>
        <div className={cls(classes.content, classes.secondContent)}>
          <Typography variant="h1" className={cls(classes.white, classes.secondH1)}>
            {'WHAT WE DO'}
          </Typography>
        </div>
      </div>

      <Container style={{padding: 0}}>
        <Grid container>
          <Grid item xs={12} sm={4} className={classes.description1}>
            <div className={classes.descriptionBox}>
              <div style={{width: '70%'}}>
                <Typography variant={"h4"}>실제적 원가절감</Typography>
                <ul>
                  <li>재무적 효과의 극대화에 최적화</li>
                  <li>평균 4-5 개월의 컨설팅 수행을 통하여 구매 및 생산비용 연간 5~15%의 절감 실현</li>
                </ul>
              </div>
            </div>
          </Grid>
          <Grid item xs={12} sm={4} className={classes.description2}>
            <div className={classes.descriptionBox}>
              <div style={{width: '70%'}}>
                <Typography variant={"h4"}>현장중심 실행 컨설팅</Typography>
                <ul>
                  <li>기업의 운영 활동 전 과정에 발생하는 비효율적 요소 개선</li>
                  <li>철저한 Fact 및 문제 해결을 통한 현장중심의 활동으로 실행력이 매우 강함</li>
                </ul>
              </div>
            </div>
          </Grid>
          <Grid item xs={12} sm={4} className={classes.description3}>
            <div className={classes.descriptionBox}>
              <div style={{width: '70%'}}>
                <Typography variant={"h4"}>검증된 방법론</Typography>
                <ul>
                  <li>다양한 경영환경에 대한 최적화 경험</li>
                  <li>다수의 국내기업에 적용되어 효과 검증</li>
                </ul>
              </div>
            </div>
          </Grid>
        </Grid>
      </Container>

      <div className={classes.header}>
        <Typography variant={"h1"} className={classes.upperLine}>
          {'CONSULTING SERVICE'}
        </Typography>
      </div>

      <Container maxWidth="lg">
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6} md={3}>
            <div className={classes.serviceBox}>
              <img src={what4} alt={"프라이빗에쿼티(PE)"} />
              <Typography variant={"h4"}>{'​프라이빗에쿼티(PE)'}</Typography>
              <GhostButton variant={'outlined'}>더 알아보기</GhostButton>
            </div>
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <div className={classes.serviceBox}>
              <img src={what5} alt={"대기업 및 중견기업"} />
              <Typography variant={"h4"}>{'​대기업 및 중견기업'}</Typography>
              <GhostButton variant={'outlined'}>더 알아보기</GhostButton>
            </div>
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <div className={classes.serviceBox}>
              <img src={what6} alt={"중소기업"} />
              <Typography variant={"h4"}>{'​중소기업'}</Typography>
              <GhostButton variant={'outlined'}>더 알아보기</GhostButton>
            </div>
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <div className={classes.serviceBox}>
              <img src={what7} alt={"벤처기업"} />
              <Typography variant={"h4"}>{'벤처기업'}</Typography>
              <GhostButton variant={'outlined'}>더 알아보기</GhostButton>
            </div>
          </Grid>
        </Grid>
      </Container>

      <div className={classes.header}>
        <Typography variant={"h1"} className={classes.upperLine}>
          {'CLIENTS'}
        </Typography>
      </div>

      <Container maxWidth="lg" style={{paddingTop: theme.spacing(4), paddingBottom: theme.spacing(4)}}>
        <Grid container justify="flex-start" spacing={3}>
          {clients.map(client => (
            <Grid key={client.id} item xs={12} sm={4} md={3} className={classes.client}>
              <img src={client.logo} alt={client.description} />
            </Grid>
          ))}
        </Grid>
      </Container>

      <div className={classes.third}>
        <div className={cls(classes.content, classes.thirdContent)}>
          <Typography variant="h1" className={cls(classes.white, classes.thirdH1)}>
            {'IEG의 CONSULTING은 오로지 '}
            <span style={{ color: '#fff'}}>{'"결과"'}</span>
            {' 로 말합니다.'}
          </Typography>
        </div>
      </div>

    </React.Fragment>
  )
}
