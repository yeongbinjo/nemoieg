import React from 'react';
import { makeStyles } from "@material-ui/core";
import bgImg from './images/1.webp';
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    backgroundImage: `url("${bgImg}")`,
    backgroundSize: 'cover',
    height: 'calc(100vh - 256px)',
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100vh - 270px)'
    }
  },
  content: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(4),
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(8),
      width: '50%',
    },
  }
}));

export default function NotFound() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.content}>
        <Typography variant="h1" style={{ color: 'white', textAlign: 'center' }}>
          {'Page Not Found'}
        </Typography>
      </div>
    </div>
  )
}
