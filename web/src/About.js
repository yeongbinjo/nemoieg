import React from 'react';
import { makeStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import cls from 'classnames';
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import case1 from './images/case1.webp';
import case2 from './images/case2.webp';
import { Parallax } from "react-parallax";
import firstBg from "./images/1.webp";
import secondBg from "./images/2.webp";
import nemoPartners from './images/nemo_partners.webp';
import about1 from './images/about1.webp';
import about2 from './images/about2.webp';
import about3 from './images/about3.webp';
import Link from "@material-ui/core/Link";

const useStyles = makeStyles(theme => ({
  white: {
    color: theme.palette.common.white,
  },
  content: {
    backgroundColor: 'rgba(0,0,0,0.38)',
    position: 'absolute',
    width: '90%',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)',
    padding: theme.spacing(4),
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(8),
    },
  },
  firstH1: {
    [theme.breakpoints.up('sm')]: {
      marginTop: theme.spacing(2),
      fontSize: 88,
      fontWeight: 800,
      textTransform: 'none',
    }
  },
  firstH4: {
    fontSize: 24,
    fontWeight: 800,
    textTransform: 'none',
  },
  firstBody: {
    marginTop: theme.spacing(3),
    lineHeight: '1.75em',
    [theme.breakpoints.up('sm')]: {
      fontWeight: 800,
      marginTop: theme.spacing(6),
      fontSize: 22,
    },
  },
  firstBody2: {
    lineHeight: '1.75em',
    [theme.breakpoints.up('sm')]: {
      fontWeight: 800,
      fontSize: 22,
    },
  },
  header: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(4),
    '& h1': {
      fontSize: 24,
    },
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(6),
      '& h1': {
        fontSize: 32,
      },
    },
  },
  upperLine: {
    flex: '0 1 auto',
    fontSize: 50,
    color: '#03013A',
    textAlign: 'center',
    fontWeight: 800,
    lineHeight: '1.75em',
    '&:before': {
      content: '""',
      display: 'block',
      width: '70px',
      margin: 'auto',
      borderTop: '3px solid #03013A',
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: 32,
      '&:before': {
        width: '42px',
      }
    },
  },
  statisticsBox: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(3),
    '& img': {
      width: 66,
      height: 66,
      margin: theme.spacing(2),
    },
    '& p': {
      color: '#03013A',
      paddingTop: theme.spacing(2),
      paddingBottom: theme.spacing(2),
      fontSize: 26,
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      fontWeight: 700,
      maxWidth: '100%',
    },
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(1),
      '& p': {
        fontSize: 18,
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1),
      }
    }
  },
  strengthBox: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
    padding: theme.spacing(3),
    '& img': {
      width: 220,
      height: 220,
      margin: theme.spacing(2),
      borderRadius: '50%',
    },
    '& h4': {
      fontSize: 28,
      color: '#03013A',
      fontWeight: 800,
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(3),
    },
    '& p': {
      color: '#03013A',
      fontSize: 20,
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      fontWeight: 400,
      maxWidth: '100%',
    },
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(1),
      '& p': {
        fontSize: 16,
      },
      '& h4': {
        fontSize: 20,
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
      },
    }
  },

  second: {
    position: 'relative',
    backgroundImage: 'url("' + secondBg + '")',
    backgroundSize: 'cover',
    height: 500,
    [theme.breakpoints.up('sm')]: {
      height: 700,
    }
  },
  secondContent: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    display: 'flex',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    transform: 'none',
    flexDirection: 'column',
    [theme.breakpoints.up('md')]: {
      flexDirection: 'row',
    },
  },
  secondLeft: {
    flex: '1 1 50%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    [theme.breakpoints.up('md')]: {
      flex: '0 0 400px',
    }
  },
  secondRight: {
    flex: '1 1 50%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    [theme.breakpoints.up('md')]: {
      flex: '1 1 auto',
      textAlign: 'left',
    }
  },
  secondH1: {
    [theme.breakpoints.up('sm')]: {
      fontSize: 70,
      fontWeight: 800,
    }
  },
  secondBody1: {
    fontWeight: 400,
    [theme.breakpoints.up('sm')]: {
      fontSize: 32,
      lineHeight: 'normal',
    },
  },
  secondBody2: {
    marginTop: theme.spacing(2),
    color: '#bbb',
    fontWeight: 400,
    [theme.breakpoints.up('sm')]: {
      marginTop: theme.spacing(4),
      fontSize: 32,
      lineHeight: 'normal',
    },
  },
  secondBody3: {
    color: '#bbb',
    fontWeight: 400,
    [theme.breakpoints.up('sm')]: {
      fontSize: 30,
      lineHeight: 'normal',
    },
  },
  partner: {
    '& img': {
      width: '100%',
      padding: theme.spacing(2),
    }
  }
}));

export default function About() {
  const classes = useStyles();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('sm'));
  const height = matches ? 1100 : 700;

  return (
    <React.Fragment>
      <Parallax
        bgImage={firstBg}
        bgImageAlt="first Image"
        strength={height}
      >
        <div style={{ height: `${height}px` }}>
          <div className={classes.content}>
            <Typography variant="h1" className={cls(classes.white, classes.firstH1)}>
              {'IEG'}
            </Typography>
            <Typography variant="h4" className={cls(classes.white, classes.firstH4)}>
              {'Industry Experts Group'}
            </Typography>
            <Typography variant="body1" className={cls(classes.white, classes.firstBody)}>
              {'"실제적인 고객가치 증대를 위한 Operation 컨설팅 전문기업입니다.'}
            </Typography>
            <Typography variant="body1" className={cls(classes.white, classes.firstBody2)}>
              {'산업전문가(Industry Expert)로서 재무적 효과로 연결되는 현장 중심의 기업가치 제고와 역량 강화 서비스를 제공하고 있습니다."'}
            </Typography>
          </div>
        </div>
      </Parallax>

      <Container maxWidth="md">
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <div className={classes.statisticsBox}>
              <img src={case1} alt={"Accumulated Savings"} />
              <Typography variant={"body1"}>717억원</Typography>
              <Typography variant={"body1"}>ACCUMULATED SAVINGS</Typography>
            </div>
          </Grid>
          <Grid item xs={12} sm={6}>
            <div className={classes.statisticsBox}>
              <img src={case2} alt={"Average Saving Rate"} />
              <Typography variant={"body1"}>9.1%</Typography>
              <Typography variant={"body1"}>AVERAGE SAVING RATE</Typography>
            </div>
          </Grid>
        </Grid>
      </Container>

      <div className={classes.header}>
        <Typography variant={"h1"} className={classes.upperLine}>
          {'STRENGTH'}
        </Typography>
      </div>

      <Container maxWidth="lg">
        <Grid container spacing={3}>
          <Grid item xs={12} sm={4}>
            <div className={classes.strengthBox}>
              <img src={about1} alt={"다양한 경험"} />
              <Typography variant={"h4"}>​다양한 경험</Typography>
              <Typography variant={"body1"}>​다양한 산업군에서</Typography>
              <Typography variant={"body1"}>원가절감 프로젝트 수행</Typography>
            </div>
          </Grid>
          <Grid item xs={12} sm={4}>
            <div className={classes.strengthBox}>
              <img src={about2} alt={"최고의 전문가"} />
              <Typography variant={"h4"}>​최고의 전문가</Typography>
              <Typography variant={"body1"}>국내 최다 원가절감</Typography>
              <Typography variant={"body1"}>프로젝트 기획 및 실행</Typography>
            </div>
          </Grid>
          <Grid item xs={12} sm={4}>
            <div className={classes.strengthBox}>
              <img src={about3} alt={"현장중심 컨설팅 수행"} />
              <Typography variant={"h4"}>​현장중심 컨설팅 수행</Typography>
              <Typography variant={"body1"}>현장 중심의</Typography>
              <Typography variant={"body1"}>Fact 확인 및 문제해결</Typography>
            </div>
          </Grid>
        </Grid>
      </Container>

      <div className={classes.header}>
        <Typography variant={"h1"} className={classes.upperLine}>
          {'PARTNER GROUP'}
        </Typography>
      </div>

      <Container maxWidth="lg" style={{marginTop: theme.spacing(1), marginBottom: theme.spacing(10)}}>
        <Grid container justify="center" spacing={3}>
          <Grid item xs={12} sm={4} md={3} className={classes.partner}>
            <Link target='_blank' href='http://www.nemopartners.com/' rel="noopener noreferrer">
              <img src={nemoPartners} alt={'네모파트너즈'} />
            </Link>
          </Grid>
        </Grid>
      </Container>

      <div className={classes.second}>
        <div className={cls(classes.content, classes.secondContent)}>
          <div className={classes.secondLeft}>
            <Typography variant="h1" className={cls(classes.white, classes.secondH1)}>
              {'IEG'}
            </Typography>
            <Typography variant="h1" className={cls(classes.white, classes.secondH1)}>
              {'Vision'}
            </Typography>
          </div>
          <div className={classes.secondRight}>
            <Typography variant="body1" className={cls(classes.white, classes.secondBody1)}>
              {'"We are the partners providing Tangible Value to client."'}
            </Typography>
            <Typography variant="body1" className={cls(classes.white, classes.secondBody2)}>
              {'고객이 직면한 어려운 문제를 함께 풀어가는 파트너로서,'}
            </Typography>
            <Typography variant="body1" className={cls(classes.white, classes.secondBody3)}>
              {'​고객 관점에서 정량적으로 증면되는 가치개선을 추구합니다.'}
            </Typography>
          </div>
        </div>
      </div>

    </React.Fragment>
  )
}
