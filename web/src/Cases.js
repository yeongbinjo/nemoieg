import React, { useEffect, useState } from 'react';
import { makeStyles, useTheme } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import cls from 'classnames';
import thirdBg from './images/3.webp';
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import case1 from './images/case1.webp';
import case2 from './images/case2.webp';
import { Link } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  third: {
    position: 'relative',
    backgroundImage: 'url("' + thirdBg + '")',
    backgroundSize: 'cover',
    height: 350,
    [theme.breakpoints.up('sm')]: {
      height: 600,
    }
  },
  thirdContent: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    display: 'flex',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    transform: 'none',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(3),
  },
  thirdH1: {
    textAlign: 'center',
    fontWeight: 'normal',
    fontSize: 24,
    lineHeight: 'normal',
    color: '#bbb',
    [theme.breakpoints.up('sm')]: {
      fontSize: 50,
    }
  },
  header: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(4),
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(6),
    },
  },
  upperLine: {
    flex: '0 1 auto',
    fontSize: 50,
    color: '#03013A',
    textAlign: 'center',
    fontWeight: 800,
    lineHeight: '1.75em',
    '&:before': {
      content: '""',
      display: 'block',
      width: '70px',
      margin: 'auto',
      borderTop: '3px solid #03013A',
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: 32,
      '&:before': {
        width: '42px',
      }
    },
  },
  statisticsBox: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(3),
    '& img': {
      width: 66,
      height: 66,
      margin: theme.spacing(2),
    },
    '& p': {
      color: '#03013A',
      paddingTop: theme.spacing(2),
      paddingBottom: theme.spacing(2),
      fontSize: 26,
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      fontWeight: 700,
      maxWidth: '100%',
    },
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(1),
      '& p': {
        fontSize: 18,
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1),
      }
    }
  },
  case: {
    width: '100%',
    display: 'inline-block',
    position: 'relative',
    backgroundSize: 'cover',
  },
  dummy: {
    paddingTop: '100%',
  },
  shadow: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  caseTitle: {
    position: 'absolute',
    left: 0,
    bottom: theme.spacing(4),
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    color: 'white',
    fontWeight: 700,
    fontSize: 20,
    margin: 0,
  },
  insight: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    textDecoration: 'none',
    color: '#000',
    width: '100%',
    height: 350,
    border: '1px solid rgba(148,144,253,0.75)',
    [theme.breakpoints.down('sm')]: {
      height: 400,
      flexDirection: 'column',
      alignItems: 'stretch',
    }
  },
  representative: {
    flex: '0 0 350px',
    backgroundSize: 'cover',
    height: 348,
    [theme.breakpoints.down('sm')]: {
      flex: '0 0 148px',
    },
  },
  right: {
    padding: theme.spacing(3),
    flex: '1 1 auto',
    height: 350,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    [theme.breakpoints.down('sm')]: {
      height: 150,
    },
  },
  insightTitle: {
    fontSize: 20,
    fontWeight: 800,
    marginBottom: theme.spacing(2),
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      height: 38,
    },
  },
  insightContent: {
    height: 240,
    '& p': {
      height: '100%',
      fontSize: 16,
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      margin: 0,
    },
    [theme.breakpoints.down('sm')]: {
      flex: '1 1 auto',
    },
  }
}));

function stripHtml(html) {
   let tmp = document.createElement("div");
   tmp.innerHTML = html;
   return tmp.textContent || tmp.innerText || "";
}

export default function Cases() {
  const classes = useStyles();
  const theme = useTheme();
  const [posts, updatePosts] = useState([]);
  const url = '/api/post/';

  useEffect(() => {
    (async function () {
      const resp = await fetch(url);
      const json = await resp.json();
      updatePosts(json);
    })();
  }, [url]);

  const cases = posts.filter(post => post.category === 'case').slice(0,4),
    insights = posts.filter(post => post.category === 'insight').slice(0,3);

  return (
    <React.Fragment>
      <div className={classes.header}>
        <Typography variant={"h1"} className={classes.upperLine}>
          {'CASE'}
        </Typography>
      </div>

      <Container maxWidth="md">
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <div className={classes.statisticsBox}>
              <img src={case1} alt={"Accumulated Savings"} />
              <Typography variant={"body1"}>717억원</Typography>
              <Typography variant={"body1"}>ACCUMULATED SAVINGS</Typography>
            </div>
          </Grid>
          <Grid item xs={12} sm={6}>
            <div className={classes.statisticsBox}>
              <img src={case2} alt={"Average Saving Rate"} />
              <Typography variant={"body1"}>9.1%</Typography>
              <Typography variant={"body1"}>AVERAGE SAVING RATE</Typography>
            </div>
          </Grid>
        </Grid>
      </Container>

      <Container maxWidth={"md"}>
        <Grid container spacing={3}>
          {cases.map(post => {
            return (
              <Grid key={post.id} item xs={12} md={6}>
                <div className={classes.case} style={{
                  backgroundImage: `url("${post.representative}")`
                }}>
                  <div className={classes.dummy} />
                  <Link className={classes.shadow} to={`/post/${post.id}/`} />
                  <p className={classes.caseTitle}>
                    {`[${post.category.charAt(0).toUpperCase() + post.category.slice(1)}] ${post.title}`}
                  </p>
                </div>
              </Grid>
            )
          })}
        </Grid>
      </Container>

      <div className={classes.header}>
        <Typography variant={"h1"} className={classes.upperLine}>
          {'INSIGHTS'}
        </Typography>
      </div>

      <Container maxWidth={"md"} style={{marginBottom: theme.spacing(8)}}>
        <Grid container spacing={3}>
          {insights.map(post => {
            return (
              <Grid key={post.id} item xs={12}>
                <Link to={`/post/${post.id}/`} className={classes.insight}>
                  <div className={classes.representative} style={{backgroundImage: `url("${post.representative}")`}} />
                  <div className={classes.right}>
                    <div className={classes.insightTitle}>{`[${post.category.charAt(0).toUpperCase() + post.category.slice(1)}] ${post.title}`}</div>
                    <div className={classes.insightContent}>
                      <p>{stripHtml(post.content)}</p>
                    </div>
                  </div>
                </Link>
              </Grid>
            )
          })}
        </Grid>
      </Container>

      <div className={classes.third}>
        <div className={cls(classes.content, classes.thirdContent)}>
          <Typography variant="h1" className={cls(classes.white, classes.thirdH1)}>
            {'IEG의 CONSULTING은 오로지 '}
            <span style={{ color: '#fff'}}>{'"결과"'}</span>
            {' 로 말합니다.'}
          </Typography>
        </div>
      </div>

    </React.Fragment>
  )
}
