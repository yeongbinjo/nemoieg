import React from 'react';
import { makeStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Container from "@material-ui/core/Container";
import clsx from "clsx";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(theme => ({
  header: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(4),
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(6),
    },
  },
  underLine: {
    flex: '0 1 auto',
    fontSize: 28,
    color: '#000',
    textAlign: 'center',
    fontWeight: 800,
    lineHeight: '2em',
    '&:after': {
      content: '""',
      display: 'block',
      width: '60px',
      margin: 'auto',
      borderBottom: '3px solid rgba(0,0,0,0.25)',
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: 22,
      '&:before': {
        width: '32px',
      }
    },
  },
  section: {
    marginBottom: theme.spacing(10),
    '& h3': {
      color: '#03013A',
      fontSize: 25,
      fontWeight: 800,
      marginTop: theme.spacing(5),
      marginBottom: theme.spacing(4),
    },
    '& li': {
      color: '#03013A',
      fontSize: 20
    },
    [theme.breakpoints.down('xs')]: {
      '& h3': {
        color: '#03013A',
        fontSize: 22,
        fontWeight: 800,
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(2),
      },
      '& li': {
        color: '#03013A',
        fontSize: 16
      },
    }
  },
  process: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: theme.spacing(6),
    marginBottom: theme.spacing(6),
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    }
  },
  circle: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    borderRadius: '50%',
    border: '12px solid #002a49',
    fontSize: 20,
    fontWeight: 800,
    width: 200,
    height: 200,
    [theme.breakpoints.down('md')]: {
      width: 160,
      height: 160,
    },
    [theme.breakpoints.down('sm')]: {
      width: 100,
      height: 100,
    },
    [theme.breakpoints.down('xs')]: {
      width: 120,
      height: 120,
    },
  },
  arrow: {
    border: 'solid #002a49',
    borderWidth: '0 5px 5px 0',
    display: 'inline-block',
    padding: 8,
    [theme.breakpoints.down('sm')]: {
      borderWidth: '0 3px 3px 0',
      padding: 4,
    }
  },
  right: {
    margin: '4px 10px 10px 4px',
    transform: 'rotate(-45deg)',
  },
  down: {
    margin: '4px 10px 10px 10px',
    transform: 'rotate(45deg)',
  },
  th: {
    '& p': {
      fontWeight: 800,
      fontSize: 20,
      marginTop: '0.7rem',
      marginBottom: '0.7rem',
    },
    '& span': {
      fontWeight: 800,
      fontSize: 16,
    },
    borderTop: '1px solid rgba(0,0,0,0.15)',
    borderBottom: '2px solid rgba(0,0,0,0.25)',
    [theme.breakpoints.down('sm')]: {
      borderTop: 'none',
      borderBottom: 'none',
      '& p': {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
      },
      '& span': {
        display: 'none',
      },
      '& div': {
        height: 96,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRight: '4px solid rgba(0,0,0,0.15)',
        marginBottom: theme.spacing(1),
      },
      '& div:nth-child(2)': {
        height: 120,
      }
    },
    [theme.breakpoints.down('xs')]: {
      '& p': {
        fontSize: 16,
      }
    },
  },
  tr: {
    '& p': {
      fontSize: 20,
      marginTop: '0.7rem',
      marginBottom: '0.7rem',
    },
    [theme.breakpoints.down('sm')]: {
      '& p': {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
      },
      '& div': {
        height: 96,
        display: 'flex',
        justifyContent: 'start',
        alignItems: 'center',
        marginBottom: theme.spacing(1),
      },
      '& div:nth-child(2)': {
        height: 120,
      }
    },
    [theme.breakpoints.down('xs')]: {
      '& p': {
        fontSize: 16,
      }
    },
  }
}));

export default function Career() {
  const classes = useStyles();
  const theme = useTheme();
  const xs = useMediaQuery(theme.breakpoints.down('xs'));

  return (
    <React.Fragment>
      <div className={classes.header}>
        <Typography variant={"h1"} className={classes.underLine}>
          {'CAREER'}
        </Typography>
      </div>

      <Container maxWidth="lg">
        <div className={classes.section}>
          <Typography variant={'h3'}>모집분야 및 자격요건</Typography>
          <Grid container>
            <Grid container item xs={4} md={12} className={classes.th}>
              <Grid item xs={12} md={3}>
                <p>모집분야 <span>Position</span></p>
              </Grid>
              <Grid item xs={12} md={6}>
                <p>자격요건 <span>Qualifications</span></p>
              </Grid>
              <Grid item xs={12} md={3}>
                <p>모집인원 <span>Volume</span></p>
              </Grid>
            </Grid>
            <Grid container item xs={8} md={12} className={classes.tr}>
              <Grid item xs={12} md={3}>
                <p>Consultant</p>
              </Grid>
              <Grid item xs={12} md={6}>
                <p>관련전공 4년제대학/대학원 졸업자 또는 졸업 예정자</p>
              </Grid>
              <Grid item xs={12} md={3}>
                <p>0 명</p>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </Container>

      <Container maxWidth="lg">
        <div className={classes.section}>
          <Typography variant={'h3'}>채용절차</Typography>
          <div className={classes.process}>
            <div className={classes.circle}>{'지원서 접수'}</div>
            <i className={clsx(classes.arrow, xs ? classes.down : classes.right)} />
            <div className={classes.circle}>{'서류심사'}</div>
            <i className={clsx(classes.arrow, xs ? classes.down : classes.right)} />
            <div className={classes.circle}>{'면접진행'}</div>
            <i className={clsx(classes.arrow, xs ? classes.down : classes.right)} />
            <div className={classes.circle}>{'채용심사'}</div>
            <i className={clsx(classes.arrow, xs ? classes.down : classes.right)} />
            <div className={classes.circle}>{'최종입사'}</div>
          </div>
        </div>
      </Container>

      <Container maxWidth="lg">
        <div className={classes.section}>
          <Typography variant={'h3'}>접수기간 및 접수방법</Typography>
          <ul>
            <li>상시채용</li>
            <li>메일접수 : hwlee@nemopartners.com</li>
            <li>1-2회 인터뷰 진행 후 OFFER 여부 결정(1-2주 소요)</li>
          </ul>
        </div>
      </Container>

      <Container maxWidth="lg">
        <div className={classes.section}>
          <Typography variant={'h3'}>제출서류</Typography>
          <ul>
            <li>Resume(이력서 및 자기소개서)</li>
            <li>접수된 Resume를 토대로 내부 심의를 거쳐 대면 인터뷰 기회 제공 예정(추가 서류 지참)</li>
            <li>결과 통보 개별 안내</li>
          </ul>
        </div>
      </Container>

      <Container maxWidth="lg" style={{marginBottom: theme.spacing(10)}}>
        <div className={classes.section}>
          <Typography variant={'h3'}>문의처</Typography>
          <ul>
            <li>02-423-0905, 3401-7001 / hwlee@nemopartners.com</li>
          </ul>
        </div>
      </Container>
    </React.Fragment>
  )
}
