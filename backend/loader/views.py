from django.http import HttpResponse
from django.shortcuts import render
from django.views import View
from meta.views import Meta


class LoaderView(View):
    """
    Serves the compiled frontend entry point
    컴파일 된 프론트엔드(React)의 엔트리포인트를 서비스하는 뷰 클래스

    (only works if you have run `yarn run build`).
    """

    def get(self, request):
        try:
            return render(request, 'index.html', {'meta': self.get_meta(request)})
        except FileNotFoundError:
            return HttpResponse(
                """
                This URL is only used when you have built the production
                version of the app. Visit http://localhost:3000/ instead, or
                run `yarn run build` to test the production version.
                """,
                status=501,
            )

    def get_meta(self, request):
        """
        현재 request를 기반으로 OpenGraph나 html meta title 등을 동적 생성하여 반환

        :param request:
        :return:
        """
        return Meta(
            title=self.get_title_from_urlpath(request),
        )

    @staticmethod
    def get_title_from_urlpath(request):
        """
        현재 URL 경로에 따른 html meta title을 리턴

        :param request:
        :return: URL 경로에 대응하는 title
        """
        if request.path_info.find('/login/') == 0:
            title = 'Nemo IEG | LOGIN'
        elif request.path_info.find('/what-we-do/') == 0:
            title = 'Nemo IEG | WHAT WE DO'
        elif request.path_info.find('/cases/') == 0:
            title = 'Nemo IEG | CASE & INSIGHTS'
        elif request.path_info.find('/career/') == 0:
            title = 'Nemo IEG | CAREER'
        elif request.path_info.find('/contact/') == 0:
            title = 'Nemo IEG | CONTACT US'
        else:
            title = 'Nemo IEG'
        return title
