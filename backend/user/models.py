from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    """유저 모델"""
    nickname = models.CharField(max_length=64)
