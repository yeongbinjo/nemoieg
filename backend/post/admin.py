from django.contrib import admin

from post.models import Post, Client


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    pass


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    pass
