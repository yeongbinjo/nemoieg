from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from post.models import Client, Post
from post.serializers import ClientSerializer, PostSerializer


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['category']
