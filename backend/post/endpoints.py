from django.conf.urls import include
from django.urls import path
from rest_framework import routers

from post.views import ClientViewSet, PostViewSet

router = routers.DefaultRouter()
router.register(r'client', ClientViewSet)
router.register(r'post', PostViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
