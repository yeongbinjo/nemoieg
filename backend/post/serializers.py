from rest_framework import serializers

from post.models import Client, Post


class ClientSerializer(serializers.ModelSerializer):
    logo = serializers.ImageField(use_url=True)
    logo_thumbnail = serializers.ImageField(use_url=True)

    class Meta:
        model = Client
        fields = ['id', 'description', 'logo', 'logo_thumbnail']


class PostSerializer(serializers.ModelSerializer):
    representative = serializers.ImageField(use_url=True)
    representative_thumbnail = serializers.ImageField(use_url=True)

    class Meta:
        model = Post
        fields = ['id', 'category', 'title', 'content', 'representative', 'representative_thumbnail']
