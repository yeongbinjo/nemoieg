from django.db import models
from froala_editor.fields import FroalaField
from imagekit.models import ImageSpecField
from model_utils import Choices
from model_utils.fields import StatusField
from model_utils.models import TimeStampedModel
from django.utils.translation import gettext_lazy as _
from pilkit.processors import Thumbnail, ResizeToFill


class Post(TimeStampedModel):
    CATEGORY = Choices('case', 'insight')
    category = StatusField(db_index=True, choices_name='CATEGORY', verbose_name=_('Category'))
    title = models.CharField(max_length=300, blank=False, null=False, verbose_name=_('Title'))
    content = FroalaField()
    representative = models.ImageField(upload_to='representative', verbose_name=_('Representative Image'))
    representative_thumbnail = ImageSpecField(
        source='representative',  # 원본 ImageField 명
        processors=[Thumbnail(450, 450)],  # 처리할 작업목록
        format='JPEG',
        options={'quality': 100},  # 저장 옵션
    )

    def __str__(self):
        return f'[{self.category.upper()}] {self.title}'


class Client(TimeStampedModel):
    description = models.TextField(blank=True, null=True, verbose_name=_('Partner\'s name'))
    logo = models.ImageField(upload_to='representative', verbose_name=_('Partner\'s logo image'))
    logo_thumbnail = ImageSpecField(
        source='logo',  # 원본 ImageField 명
        processors=[ResizeToFill(220, 160)],  # 처리할 작업목록
        format='JPEG',
        options={'quality': 100},  # 저장 옵션
    )
    ordinal = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.description
