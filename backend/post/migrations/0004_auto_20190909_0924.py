# Generated by Django 2.2.5 on 2019-09-09 00:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('post', '0003_partner'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Partner',
            new_name='Client',
        ),
    ]
